public abstract class GridPoint {

	protected final boolean mutable;

	//constructors
	public GridPoint() {
		mutable = true;
	}
	public GridPoint(boolean mutable){
		this.mutable = mutable;
	}
	public GridPoint(GridPoint point){
		this.mutable = point.mutable;
	}
	
	//getters
	public abstract double getValue();
	
	public boolean isMutable(){
		return mutable;
	}
	
	public abstract String toString();

	//setters
	public abstract void setValue(double value);
}