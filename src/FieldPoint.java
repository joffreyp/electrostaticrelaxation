/**
 * 
 * 
 * @author joffrey
 *
 *@mutable boolean determining whether or not the point is mutable
 *@potential double representing the potential value at the point
 */
public class FieldPoint extends GridPoint {
	
	private double x;
	private double y;
	
	//constructors
	public FieldPoint(){
		super();
		x = 0;
		y = 0;
	}
	public FieldPoint(boolean mutable, double x, double y) {
		super(mutable);
		this.x = x;
		this.y = y;
	}
	public FieldPoint(FieldPoint point){
		super(point);
		this.x = point.x;
		this.y = point.y;
	}
	
	//getters
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public double getValue(){
		return Math.sqrt(x*x+y*y);
	}
	
	//setters
	public void setX(double x){
		this.x = x;
	}
	public void setY(double y){
		this.y = y;
	}
	public void setValue(double value){//this is a silly function
		x = value;
		y = value;
	}
	
	public String toString(){
		return Double.toString(x) + "\t" + Double.toString(y);
	}
	
}
