/**
 * 
 * 
 * @author joffrey
 *
 *@mutable boolean determining whether or not the point is mutable
 *@potential double representing the potential value at the point
 */
public class PotentialPoint extends GridPoint {

	private double potential;
	//constructors
	public PotentialPoint(){
		super();
		potential = 0;
	}
	public PotentialPoint(boolean mutable, double potential) {
		super(mutable);
		this.potential = potential;
	}
	public PotentialPoint(PotentialPoint point){
		super(point);
	}
	
	//setters
	public void setPotential(double potential){
		this.potential = potential;
	}
	public void setValue(double value){
		this.potential = value;
	}
	//getters
	public double getValue(){
		return potential;
	}
	public double getPotential(){
		return potential;
	}
	
	public String toString(){
		return Double.toString(potential);
	}
	
}
