import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;


public class Relaxation {

	/**
	 * @param args
	 * 
	 * This is the main class for the elecrostatic relaxation simulation.
	 * The simulation is based on the 2D grid electrostatic relaxation simulation outlined
	 * in Jackson's Classical Electrodynamics (3rd Ed.), in Ch. 1, Sec. 1.13, pp. 47-50.
	 * 
	 * 
	 */
	private static double differenceLimit = 0.0001;
	private static final int m = 1000;//1000= 1000mm
	private static final int n = 1000;
	private static final double voltage = 100.0; //voltage on each plate; one at +voltage, one at -voltage
	
	public static void main(String[] args) {
		Date date = new Date();
		Grid grid = new Grid(m,n);
		grid.initializePotentialGrid();
		String finalGrid = "C:\\jkp\\Data\\finalGridThin1000.dat";
		String gradient = "C:\\jkp\\Data\\gradientThin1000.dat";
		
		//define potential configuration; 1 = 1 mm, or 100 = 10cm
		//two plates, 1cm wide, 5 cm tall, spaced 5 cm apart, 1 mm horizontal optical thru port gap in plates
		//top left
		grid.lockSection(424, 450, 459, 474, voltage);
		//top right
		grid.lockSection(424, 449, 524, 539, voltage);
		//bottom left
		grid.lockSection(549, 574, 459, 474, -voltage);
		//bottom right
		grid.lockSection(549, 574, 524, 539, -voltage);

		grid.relax(differenceLimit);
		//relax(grid,10000);
		
		try{
			// Create file 
			FileWriter fstream = new FileWriter(finalGrid);
			BufferedWriter out = new BufferedWriter(fstream);
			String newGridString = grid.toString();
			out.write(newGridString,0,newGridString.length());	
			//Close the output stream
			out.close();
		}
		catch(Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			System.out.println("Error: " + e.getMessage());
		}
		
		//make gradient grid
		Grid grad = grid.gradientGrid(0.1);
		try{
			// Create file 
			FileWriter fstream = new FileWriter(gradient);
			BufferedWriter out = new BufferedWriter(fstream);
			String gradString = grad.toString();
			out.write(gradString,0,gradString.length());	
			//Close the output stream
			out.close();
		}
		catch(Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			System.out.println("Error: " + e.getMessage());
		}
		System.out.println("Runtime in s: " + ((new Date()).getTime()-date.getTime())/1000);
	}
}
