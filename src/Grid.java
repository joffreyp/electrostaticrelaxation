
public class Grid {

	//rows
	private final int m;
	//columns
	private final int n;
	//2D array of GridPoint objects
	private GridPoint[][] grid;
	
	
	public Grid(int m, int n) {
		super();
		this.m = m;
		this.n = n;
		grid = new GridPoint[m][n];
	}
	public Grid(Grid grid) {
		super();
		this.m = grid.getM();
		this.n = grid.getN();
		this.grid = new GridPoint[m][n];
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				this.grid[i][j]=grid.getGridPoint(i, j);
			}
		}
	}
	
	//getters
	public int getM() {
		return m;
	}

	public int getN() {
		return n;
	}

	public GridPoint getGridPoint(int row, int col) {
		if(row >= 0 && col >= 0 && row < m && col < n){
			return grid[row][col];
		}
		else return null;
	}


	public double getCross(int row, int col){
		double returnValue = 0;
		//TODO: Is ignoring edge points the correct thing to do?
		if(row >= 0 && row < m && col >= 0 && col < n && grid[row][col].isMutable()){
			if(row > 0){
				returnValue += grid[row-1][col].getValue();
			}
			if(row < (m-1)){
				returnValue += grid[row+1][col].getValue();
			}
			if(col > 0){
				returnValue += grid[row][col-1].getValue();
			}
			if(col < (n-1)){
				returnValue += grid[row][col+1].getValue();
			}
		}
		return returnValue/4;
	}
	
	public double getSquare(int row, int col){
		double returnValue = 0;
		
		if(row >= 0 && row < m && col >= 0 && col < n && grid[row][col].isMutable()){
			if(row > 0){
				if(col > 0){
					returnValue += grid[row-1][col-1].getValue();
				}
				if(col < (n-1)){
					returnValue += grid[row-1][col+1].getValue();
				}
			}
			if(row < (m-1)){
				if(col > 0){
					returnValue += grid[row+1][col-1].getValue();
				}
				if(col < (n-1)){
					returnValue += grid[row+1][col+1].getValue();
				}
			}
		}
		return returnValue/4;
	}
	
	//setters
	public boolean setGridPoint(int row, int col, double value) {
		if(row > 0 && col > 0 && row < m && col < n && grid[row][col].isMutable()){
			grid[row][col].setValue(value);
			return true;
		}
		return false;
	}
	public boolean setGridPoint(int row, int col, GridPoint point) {
		if(row > 0 && col > 0 && row < m && col < n){
			grid[row][col]=point;
			return true;
		}
		return false;
	}
	public boolean setGridPoint(int row, int col, FieldPoint point) {
		if(row > 0 && col > 0 && row < m && col < n){
			grid[row][col]=point;
			return true;
		}
		return false;
	}
	
	public void initializeFieldGrid(){
		//this.grid = new FieldPoint[m][m];
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				this.grid[i][j]=new FieldPoint();
			}
		}
	}
	public void initializePotentialGrid(){
		//this.grid = new FieldPoint[m][m];
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				this.grid[i][j]=new PotentialPoint();
			}
		}
	}
	
	//functionality
	public boolean lockSection(int rowStart, int rowEnd, int colStart, int colEnd, double voltage){
		if(rowStart >= 0 && rowStart < m && rowEnd >= 0 && rowEnd < m 
				&& colStart >= 0 && colStart < n && colEnd >=0 && colEnd < n){
			
			for(int i = rowStart; i <= rowEnd; i++){
				for(int j = colStart; j <= colEnd; j++){
					grid[i][j] = new PotentialPoint(false,voltage);
				}
			}
			return true;
		}
		return false;
	}
	
	public Grid relax(double differenceLimit){
		
		double newPotential;
		boolean limitReached = false;
		int c = 0;
		
		while(!limitReached){
			c++;
			limitReached = true;
			for(int i = 0; i < m; i++){
				for(int j = 0; j < n; j++){
					
					if(grid[i][j].isMutable()){
						newPotential = 4*getCross(i, j)/5 + getSquare(i, j)/5; //from Jackson p. 48
						if(Math.abs(newPotential-grid[i][j].getValue()) > differenceLimit){
							limitReached = false;
						}
						grid[i][j].setValue(newPotential);
					}
				}
			}
		}
		System.out.println("Difference Limit: " + differenceLimit + "   Iterations: " + c);
	
		return this;
	}
	
	public String toString(){
		String output = "";
		for(int i = 0; i < m; i++){
			output+=toString(i);
			output+="\n";
		}
		return output;
	}
	
	public String toString(int row){
		String output = "";
		if(row >= 0 && row <= m){
			for(int i = 0; i < m; i++){
				output+=grid[row][i].toString();
				output+="\t";
			}
		}
		return output;
	}
	
	public Grid gradientGrid(double scale){
		Grid  grad = new Grid(this.m, this.n);
		grad.initializeFieldGrid();
		
		double x; // horizontal, increasing j
		double y; // vertical, decreasing i
		
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				x = 0;
				y = 0;
				if(grid[i][j].isMutable()){
					if(i > 0){
						y += (grid[i-1][j].getValue() - grid[i][j].getValue())/scale; //S
					}
					if(i < (m-1)){
						y -= (grid[i+1][j].getValue() - grid[i][j].getValue())/scale;//N
					}
					if(j > 0){
						x -= (grid[i][j-1].getValue() - grid[i][j].getValue())/scale;//W
					}
					if(j < (n-1)){
						x += (grid[i][j+1].getValue() - grid[i][j].getValue())/scale;//E
					}
					
					grad.setGridPoint(i, j, new FieldPoint(true, x, y));
				}
			}
		}
		return grad;
	}
}
